import api from '../../api';

import React, { useState, useEffect } from 'react';
import { Container, AppBar, Box, Typography } from '@mui/material';
import { Outlet } from 'react-router';

function Main() {
  const [cards, setCards] = useState([]);
  const [card, setCard] = useState(null);
  const [editCardId, setEditCardId] = useState(null);
  const [editField, setEditField] = useState(null);
  const [loading, setLoading] = useState(true);

  function handleEditClick(id, field) {
    setEditCardId(id);
    setEditField(field);
  }

  function handleCancelClick() {
    setEditCardId(null);
  }

  const fetchProducts = async () => {
    try {
      const response = await api.get('');

      if (response.data.success === true) {
        setLoading(false);
        const fetchedData = response.data.data;
        return setCards(fetchedData);
      }
    } catch (error) {
      console.error('Error fetching products:', error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const handleFormSubmit = async (formData) => {
    const { name, description, price, quantity } = formData;
    try {
      await api.post('', {
        name: name,
        description: description,
        price: price,
        quantity: quantity,
      });

      fetchProducts();
      setLoading(false);
    } catch (error) {
      console.error('Error adding card:', error);
      setLoading(false);
    }
  };

  const handleFullPageFormSubmit = async (event, formData, id) => {
    event.preventDefault();

    setEditCardId(null);

    const { name, description } = formData;
    try {
      const response = await api.put(`/${id}`, {
        name: name,
        description: description,
      });

      setCard(response.data.data);
      fetchProducts();
      setLoading(false);
    } catch (error) {
      console.error('Error adding full page edted card:', error);
      setLoading(false);
    }
  };

  const handleDelete = async (id) => {
    try {
      await api.delete(`/${id}`);

      setCards((prevFormList) => prevFormList.filter((card) => card.id !== id));
      setLoading(false);
    } catch (error) {
      console.error('Error deleting form data:', error);
      setLoading(false);
    }
  };

  const handleEditFormSubmit = async (event, form, id) => {
    event.preventDefault();

    setEditCardId(null);

    try {
      const { name, description } = form;
      await api.patch(`/${id}`, {
        name: name,
        description: description,
      });

      const updatedCards = cards.map((card) => {
        if (card.id === form.id) {
          return { ...card, name: name, description: description };
        }
        return card;
      });
      setCards(updatedCards);
      setLoading(false);
    } catch (error) {
      console.error('Error editing form data:', error);
      setLoading(false);
    }
  };

  const cardToEdit = cards.find((card) => card.id === editCardId);

  if (loading) {
    return (
      <Box
        sx={{
          margin: 0,
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        Loading...
      </Box>
    );
  }

  return (
    <>
      <AppBar
        position="sticky"
        sx={{
          color: '#4B4B4D',
          bgcolor: 'white',
          justifyContent: 'center',
          width: '100%',
          height: '70px',
        }}
        elevation={1}
      >
        <Box display="flex" justifyContent="center">
          <Typography variant="h4" py={4}>
            <strong>ADMIN PANEL</strong>
          </Typography>
        </Box>
      </AppBar>
      <Container maxWidth="lg">
        <Outlet
          context={{
            cards,
            setCards,
            handleEditFormSubmit,
            editCardId,
            handleEditClick,
            handleCancelClick,
            editField,
            handleFormSubmit,
            handleDelete,
            cardToEdit,
            handleFullPageFormSubmit,
            card,
            setCard,
            loading,
            setLoading,
          }}
        />
      </Container>
    </>
  );
}

export default Main;
