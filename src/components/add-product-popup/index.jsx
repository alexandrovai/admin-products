import {
  Dialog,
  Box,
  IconButton,
  DialogContent,
  Grid,
  Divider,
  Typography,
  Stack,
  Button,
} from '@mui/material';
import React from 'react';
import CloseIcon from '@mui/icons-material/Close';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import InputField from '../input-field';
import { useOutletContext } from 'react-router';

function AddProductPopup({ openModal, handleClickModalClose }) {
  const { handleFormSubmit } = useOutletContext();

  const validationSchema = Yup.object().shape({
    name: Yup.string().required('Please enter product name'),
    description: Yup.string().required('Please enter product description'),
    quantity: Yup.number().required('Please enter product quantity'),
    price: Yup.number().required('Please enter product price'),
  });

  const defaultValues = {
    name: '',
    description: '',
    quantity: '',
    price: '',
  };

  const { control, handleSubmit, reset } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });

  function onSubmit(data) {
    const { name, description, quantity, price } = data;
    const formData = { name, description, price, quantity };

    handleFormSubmit(formData);
    reset();
    handleClickModalClose();
  }

  return (
    <Dialog
      open={openModal}
      onClose={handleClickModalClose}
      sx={{ minWidth: '900px', minHeight: '100vh' }}
    >
      <Box sx={{ width: '100%' }}>
        <IconButton
          aria-label="close"
          onClick={handleClickModalClose}
          sx={{
            position: 'absolute',
            right: 6,
            top: 6,
            fontSize: 20,
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent sx={{ minWidth: '600px', minHeight: '80vh' }}>
          <Grid container flexDirection="column" rowGap={2}>
            <Grid item xs={12} alignItems="center" justifyContent="center">
              <Typography>ADD NEW PRODUCT DATA:</Typography>
            </Grid>

            <Grid
              item
              xs={12}
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
            >
              <Divider
                orientation="horizontal"
                style={{ width: '100%', border: '1px solid grey' }}
              />
            </Grid>
            <Box
              component="form"
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit(onSubmit)}
              sx={{ padding: '10px' }}
            >
              <Stack spacing={2} direction="column" sx={{ marginBottom: 4 }}>
                <InputField
                  name="name"
                  control={control}
                  label="Product Name"
                  id="name"
                />
                <InputField
                  name="description"
                  control={control}
                  label="Product Description"
                  id="description"
                />
                <InputField
                  name="quantity"
                  control={control}
                  label="Product Quantity"
                  id="quantity"
                />
                <InputField
                  name="price"
                  control={control}
                  label="Product Price"
                  id="price"
                />
              </Stack>

              <Button variant="outlined" color="secondary" type="submit">
                Submit
              </Button>
            </Box>
          </Grid>
        </DialogContent>
      </Box>
    </Dialog>
  );
}

export default AddProductPopup;
