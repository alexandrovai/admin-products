import React, { useState, useEffect } from 'react';
import {
  Grid,
  Card,
  Typography,
  Button,
  CardContent,
  CardActions,
  Pagination,
} from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useOutletContext } from 'react-router-dom';

import ProductCard from '../product-card';
import ProductCardToEdit from '../product-card-to-edit';
import AddProductPopup from '../add-product-popup';
import { CARDS_PER_PAGE } from '../../constants';

function ProductList() {
  const { cards, cardToEdit, editCardId, editField, handleEditFormSubmit } =
    useOutletContext();

  const [currentPage, setCurrentPage] = useState(1);
  const [currentCards, setCurrentCards] = useState([]);
  const [openModal, setModal] = useState(false);

  const handleClickModalOpen = () => {
    setModal(true);
  };

  const handleClickModalClose = () => {
    setModal(false);
  };

  useEffect(() => {
    const indexOfLastCard = currentPage * CARDS_PER_PAGE;
    const indexOfFirstCard = indexOfLastCard - CARDS_PER_PAGE;
    const currentCardsSlice = cards.slice(indexOfFirstCard, indexOfLastCard);
    setCurrentCards(currentCardsSlice);
  }, [currentPage, cards, CARDS_PER_PAGE]);

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  return (
    <Grid
      container
      align="center"
      alignItems="center"
      justifyContent="center"
      mt={4}
      spacing={4}
    >
      <Card sx={{ marginBottom: '50px', width: '25%' }}>
        <CardContent>
          <CardActions
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Typography gutterBottom variant="h6" component="h2">
              ADD PRODUCT
            </Typography>
            <Button
              variant="outlined"
              sx={{
                color: 'grey',
                border: '1px solid rgba(0, 0, 0, 0.24);',
                width: '150px',
              }}
              onClick={handleClickModalOpen}
            >
              <AddCircleOutlineIcon />
            </Button>

            <AddProductPopup
              openModal={openModal}
              handleClickModalClose={handleClickModalClose}
            />
          </CardActions>
        </CardContent>
      </Card>

      <Grid container spacing={2} minHeight="100vh">
        {currentCards.map((card) => (
          <Grid item key={card.id} xs={3}>
            {editCardId === card.id ? (
              <ProductCardToEdit
                data={card}
                isArrowShown={true}
                defaultValue={editCardId !== null && cardToEdit}
                editField={editField}
                isDeleteiconShown={true}
                handleFormSubmit={handleEditFormSubmit}
              />
            ) : (
              <ProductCard
                data={card}
                isArrowShown={true}
                isDeleteiconShown={true}
              />
            )}
          </Grid>
        ))}
      </Grid>
      <Grid item mb={4}>
        <Pagination
          count={Math.ceil(cards.length / CARDS_PER_PAGE)}
          page={currentPage}
          onChange={handlePageChange}
          showFirstButton
          showLastButton
        />
      </Grid>
    </Grid>
  );
}

export default ProductList;
