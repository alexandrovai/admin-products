import React from 'react';
import { IconButton, TextField, InputAdornment, Tooltip } from '@mui/material';

import { Controller } from 'react-hook-form';
import ClearIcon from '@mui/icons-material/Clear';

function InputField({ name, control, id, label }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <TextField
          fullWidth
          id={id}
          label={label}
          variant="outlined"
          helperText={error ? error.message : null}
          error={!!error}
          onChange={onChange}
          value={value || ''}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Tooltip placement="top" title="Clear input">
                  <IconButton
                    onClick={() => onChange('')}
                    sx={{
                      cursor: 'pointer',
                      padding: 0,
                    }}
                  >
                    <ClearIcon
                      fontSize="small"
                      sx={{ width: '1rem', height: '1rem' }}
                    />
                  </IconButton>
                </Tooltip>
              </InputAdornment>
            ),
          }}
        />
      )}
    />
  );
}

export default InputField;
