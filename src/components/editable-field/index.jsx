import React from 'react';
import {
  TextField,
  Typography,
  Grid,
  Box,
  Button,
  IconButton,
} from '@mui/material';
import { useOutletContext } from 'react-router';
import UndoOutlinedIcon from '@mui/icons-material/UndoOutlined';
import EditIcon from '@mui/icons-material/Edit';

function EditableField({ fieldName, handleChange, defaultValue, formValue }) {
  const { handleCancelClick, handleEditClick, editField } = useOutletContext();

  return (
    <Grid
      container
      justifyContent="center"
      flexDirection="column"
      alignItems="flex-end"
      rowGap={0.5}
    >
      {editField === fieldName ? (
        <>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%',
            }}
          >
            <span>{fieldName}: </span>
            <TextField
              sx={{
                whiteSpace: 'nowrap',
                width: '100%',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                padding: 0,
                textAlign: 'end',
                marginLeft: '20px',
              }}
              InputProps={{ sx: { height: 30 } }}
              size="small"
              name={fieldName}
              value={formValue}
              onChange={handleChange}
            />
          </Box>

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              gap: '10px',
              marginBottom: '12px',
            }}
          >
            <Button
              variant="contained"
              sx={{
                bgcolor: 'white',
                borderRadius: '8px',
                color: '#4B4B4D',
                padding: 0,
                ':hover': {
                  bgcolor: '#D9D9D9',
                },
              }}
              type="reset"
              onClick={handleCancelClick}
            >
              <UndoOutlinedIcon />
            </Button>
            <Button
              variant="contained"
              sx={{
                bgcolor: 'white',
                borderRadius: '8px',
                color: '#4B4B4D',
                padding: 0,
                ':hover': {
                  bgcolor: '#D9D9D9',
                },
              }}
              type="submit"
            >
              SAVE
            </Button>
          </Box>
        </>
      ) : (
        <Grid
          container
          justifyContent="flex-end"
          flexDirection="row"
          width="100%"
          alignItems="center"
          gap={1}
        >
          <Typography
            sx={{
              overflow: 'hidden',
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              maxWidth: '80%',
            }}
          >{`${fieldName}: ${defaultValue}`}</Typography>

          <IconButton
            sx={{ padding: 0 }}
            onClick={() => handleEditClick(fieldName, defaultValue)}
          >
            <EditIcon fontSize="small" />
          </IconButton>
        </Grid>
      )}
    </Grid>
  );
}

export default EditableField;
