import truncateString from '../../utils/truncate-string';
import DefaultImage from '../../images/coming-soon.jpg';

import React from 'react';
import {
  Card,
  CardContent,
  Typography,
  Divider,
  Grid,
  Box,
  IconButton,
} from '@mui/material';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { Link as MuiLink } from '@mui/material';
import { Link, useOutletContext } from 'react-router-dom';
import EditIcon from '@mui/icons-material/Edit';
import CardMedia from '@mui/material/CardMedia';

function ProductCard({ data, isArrowShown, isDeleteiconShown }) {
  const { handleDelete, handleEditClick } = useOutletContext();

  return (
    <Card sx={{ minHeight: '320px' }}>
      <CardContent>
        {isDeleteiconShown && (
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginBottom: '10px',
            }}
          >
            <IconButton
              sx={{ padding: 0 }}
              onClick={() => handleDelete(data.id)}
            >
              <DeleteOutlineIcon fontSize="small" />
            </IconButton>
          </Box>
        )}

        <Grid
          container
          justifyContent="center"
          flexDirection="column"
          alignItems="center"
          rowGap={1}
        >
          <Grid item>
            <Grid
              container
              justifyContent="flex-end"
              flexDirection="row"
              width="100%"
              alignItems="center"
              gap={1}
            >
              <Typography
                sx={{
                  whiteSpace: 'nowrap',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                }}
              >
                {truncateString(data.name.toUpperCase(), 15)}
              </Typography>

              <IconButton
                sx={{ padding: 0 }}
                onClick={() => handleEditClick(data.id, 'name')}
              >
                <EditIcon fontSize="small" />
              </IconButton>
            </Grid>
          </Grid>

          <Grid item>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Grid
                container
                flexDirection="row"
                alignItems="center"
                justifyContent="center"
                gap={1}
                mb={1}
              >
                <Typography
                  sx={{
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                  }}
                >{`description: ${truncateString(
                  data.description,
                  10,
                )}`}</Typography>

                <IconButton
                  sx={{ padding: 0 }}
                  onClick={() => handleEditClick(data.id, 'description')}
                >
                  <EditIcon fontSize="small" />
                </IconButton>
              </Grid>

              <CardMedia
                sx={{ height: 140, width: 140 }}
                image={DefaultImage}
                title="pic"
              />
            </Box>
          </Grid>
        </Grid>

        <Divider sx={{ margin: '20px auto' }} />

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}
        >
          <Typography
            gutterBottom
            component="span"
            sx={{
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            }}
          >
            {`quantity: ${data.quantity}`}
          </Typography>
          <Typography
            component="span"
            sx={{
              overflow: 'hidden',
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
            }}
          >{`price: ${data.price}`}</Typography>
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
          }}
        >
          {isArrowShown && (
            <MuiLink
              component={Link}
              target="_blank"
              rel="noopener noreferrer"
              to={`/${data.id}`}
              underline="none"
              sx={{ cursor: 'ponter', marginTop: '20px' }}
            >
              <ArrowCircleRightOutlinedIcon />
            </MuiLink>
          )}
        </Box>
      </CardContent>
    </Card>
  );
}

export default ProductCard;
